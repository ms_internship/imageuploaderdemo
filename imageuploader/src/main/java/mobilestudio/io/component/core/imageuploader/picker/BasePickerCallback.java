package mobilestudio.io.component.core.imageuploader.picker;


import android.net.Uri;

/**
 * Created by Sayed on 8/16/2017.
 */

public interface BasePickerCallback {

    void onUriReceived(Uri resultUri);

}
