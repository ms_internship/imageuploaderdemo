package mobilestudio.io.component.core.imageuploader.picker;

import java.util.List;

/**
 * Created by Sayed on 8/16/2017.
 */

public interface PermissionCallback {

    void onPermissionResult(List<String> grantedList, List<String> deniedList);


}
