package mobilestudio.io.component.core.imageuploader.uploader;

import android.net.Uri;

/**
 * Created by Sayed on 8/20/2017.
 */

public interface Uploader {

     void upload(String endPoint, Uri uri);
}
