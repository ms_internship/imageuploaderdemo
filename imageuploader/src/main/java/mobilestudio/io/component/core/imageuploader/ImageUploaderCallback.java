package mobilestudio.io.component.core.imageuploader;

/**
 * Created by Sayed on 8/27/2017.
 */

public interface ImageUploaderCallback {

    void onUploadSuccess();

    void onUploadFailed(String errorMessage);

}
