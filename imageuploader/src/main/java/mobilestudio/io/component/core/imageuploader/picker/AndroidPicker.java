package mobilestudio.io.component.core.imageuploader.picker;

import android.content.Intent;

/**
 * Created by Sayed on 8/29/2017.
 */

public interface AndroidPicker extends Picker {

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onPermissionRequestResult(int requestCode,
                                   String permissions[], int[] grantResults);
}
