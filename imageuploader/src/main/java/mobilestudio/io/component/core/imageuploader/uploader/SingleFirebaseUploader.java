package mobilestudio.io.component.core.imageuploader.uploader;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.UploadTask;

import mobilestudio.io.component.core.imageuploader.ImageUploaderCallback;

/**
 * Created by Sayed on 8/20/2017.
 */

public class SingleFirebaseUploader extends FirebaseUploader {

    ImageUploaderCallback callback;

    public SingleFirebaseUploader(ImageUploaderCallback callback) {
        this.callback = callback;
    }

    @Override
    public void upload(String endPoint, Uri uri) {



        firebaseStorage.getReferenceFromUrl(endPoint)
                .putFile(uri).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onUploadFailed(e.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                callback.onUploadSuccess();
            }
        });
    }

}

