package mobilestudio.io.component.core.imageuploader.picker;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by Sayed on 8/17/2017.
 */

public abstract class BaseAndroidImagePicker implements AndroidPicker {
    Activity actvity;
    Intent intent;

    final static int IMAGE_PICKER_REQUEST_CODE = 1;
    final static int RESULT_OK = Activity.RESULT_OK;

    public BaseAndroidImagePicker(Activity activity) {
        this.actvity = activity;
        intent = new Intent();
    }

    public Intent getIntent() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        return intent;
    }



}
