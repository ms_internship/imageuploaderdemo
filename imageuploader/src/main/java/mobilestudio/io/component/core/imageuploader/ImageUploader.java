package mobilestudio.io.component.core.imageuploader;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;

import mobilestudio.io.component.core.imageuploader.picker.AndroidPicker;
import mobilestudio.io.component.core.imageuploader.picker.BasePickerCallback;
import mobilestudio.io.component.core.imageuploader.uploader.Uploader;

/**
 * Created by Sayed on 8/27/2017.
 */

public class ImageUploader implements BasePickerCallback, IImageUploader{
    Uploader uploader;
    AndroidPicker picker;
    Uri uri;

    public ImageUploader(AndroidPicker imagePicker , Uploader firebaseUploader){
        this.picker = imagePicker;
        imagePicker.addCallback(this);
        this.uploader = firebaseUploader;

    }


    @Override
    public void onUriReceived(Uri resultUri) {
        uri = resultUri;
    }

    @Override
    public void upload(String endPoint) {
        if(uri != null)
        uploader.upload(endPoint , uri);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        picker.onActivityResult(requestCode ,resultCode , data);
    }

    @Override
    public void onPermissionRequestResult(int requestCode, String[] permissions, int[] grantResults) {
        picker.onPermissionRequestResult(requestCode , permissions , grantResults);
    }
}
