package mobilestudio.io.component.core.imageuploader.picker;

import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import mobilestudio.io.component.core.imageuploader.R;


/**
 * Created by Sayed on 8/21/2017.
 */

public class DefaultImagesPickerView extends LinearLayout {
    TextView textView;
    ImageView imageView;
    ImageView handlerImage;
    TypedArray typedArray;


    public DefaultImagesPickerView(Context context) {
        super(context);
    }

    public DefaultImagesPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DefaultImagesPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }


    public void init(Context context, int imageViewId, int handlerImageId, int textViewId) {

        inflate(context, R.layout.image_picker_item, this);
        textView = findViewById(textViewId);
        imageView = findViewById(imageViewId);
        handlerImage = findViewById(handlerImageId);
    }

    @Override
    public void setBackgroundColor(int colorId) {
        textView.setBackgroundColor(colorId);
        handlerImage.setBackgroundColor(colorId);
        imageView.setBackgroundColor(colorId);

    }

    public void setTitleText(String text) {
        textView.setText(text);
    }

    public void setPlaceHolder(int Id) {

        handlerImage.setImageResource(Id);
    }

    public ImageView getHandlerImage() {
        return handlerImage;
    }

    public void setImage(Uri uri) {

        imageView.setImageURI(uri);

    }


}
