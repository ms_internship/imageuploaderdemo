package mobilestudio.io.component.core.imageuploader.picker;

import android.content.Intent;

/**
 * Created by Sayed on 8/16/2017.
 */

public interface Picker {

    void openPicker();

    void addCallback(BasePickerCallback callback);



}
