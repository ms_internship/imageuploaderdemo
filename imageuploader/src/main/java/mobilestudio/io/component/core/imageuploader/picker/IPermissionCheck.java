package mobilestudio.io.component.core.imageuploader.picker;

/**
 * Created by Sayed on 8/15/2017.
 */

public interface IPermissionCheck {



    void requestPermissions(String... permissionsRequested);

    void onPermissionRequestResult(int requestCode,
                                   String permissions[], int[] grantResults);

}
