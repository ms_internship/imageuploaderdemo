package mobilestudio.io.component.core.imageuploader;

import android.content.Intent;

/**
 * Created by Sayed on 8/27/2017.
 */

public interface IImageUploader {

    void upload(String endPoint);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onPermissionRequestResult(int requestCode,
                                   String permissions[], int[] grantResults);

}
