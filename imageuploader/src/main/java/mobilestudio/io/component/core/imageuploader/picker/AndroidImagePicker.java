package mobilestudio.io.component.core.imageuploader.picker;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobilestudio.io.component.core.imageuploader.R;


/**
 * Created by Sayed on 8/17/2017.
 */

public class AndroidImagePicker extends BaseAndroidImagePicker implements PermissionCallback {

    private final String STORAGE_PERMISSION = Manifest.permission.READ_EXTERNAL_STORAGE;
    IPermissionCheck permissionCheck;
    private List<BasePickerCallback> callbacks;
    private String titleText;
    private int backgroundColor;
    private int placeHolder;

    private int customViewId;
    private int imageViewId;
    private Integer defaultViewId;
    private int handlerId;
    private int titleId;


    TextView textView;
    View containerView;
    ImageView imageView;
    ImageView handlerImage;

    DefaultImagesPickerView defaultView;


    private AndroidImagePicker(Builder builder) {
        super(builder.activity);
        callbacks = new ArrayList<>();
        titleText = builder.titleText;
        backgroundColor = builder.backgroundColor;
        placeHolder = builder.placeHolder;
        customViewId = builder.customViewId;
        imageViewId = builder.imageViewId;
        handlerId = builder.handlerId;
        titleId = builder.titleId;

        defaultViewId = builder.defaultViewID;
        permissionCheck = new PermissionCheck(builder.activity, this);

        try {
            if (defaultViewId != null) {
                defaultView = actvity.findViewById(defaultViewId);
                defaultView.init(actvity, imageViewId, handlerId, titleId);
            }

            textView = actvity.findViewById(titleId);
            imageView = actvity.findViewById(imageViewId);
            handlerImage = actvity.findViewById(handlerId);
            handlerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openPicker();
                }
            });
            textView.setText(titleText);
            handlerImage.setImageResource(placeHolder);
            containerView.setBackgroundColor(backgroundColor);

        } catch (NullPointerException e) {
            Log.i("defaultViewError ", e.getMessage());
        }

    }


    @Override
    public void openPicker() {

        permissionCheck.requestPermissions(STORAGE_PERMISSION);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && data != null) {

            if (data.getData() != null) {

                Uri imageUri = data.getData();

                imageView.setImageURI(imageUri);

                for (BasePickerCallback callback : callbacks) {
                    if (callback != null)
                        callback.onUriReceived(imageUri);
                }

            }
        }
    }

    @Override
    public void addCallback(BasePickerCallback callback) {
        callbacks.add(callback);
    }

    @Override
    public void onPermissionRequestResult(int requestCode, String[] permissions, int[] grantResults) {
        permissionCheck.onPermissionRequestResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(List<String> grantedList, List<String> deniedList) {
        for (String grantedPermission : grantedList) {
            if (grantedPermission.equals(STORAGE_PERMISSION)) {
                actvity.startActivityForResult(getIntent(), IMAGE_PICKER_REQUEST_CODE);

            }
        }

    }


    public static class Builder {

        Activity activity;

        private int backgroundColor = Color.WHITE;
        private int placeHolder = R.drawable.pick_image;

        private int customViewId;
        private Integer defaultViewID;
        private int imageViewId = R.id.image_selected;
        private int handlerId = R.id.handler;
        private int titleId = R.id.image_title;
        private String titleText = "Images Picker";


        public Builder(Activity activity) {
            this.activity = activity;
        }


        public Builder defaultView(int defaultViewId) {
            this.defaultViewID = defaultViewId;
            return this;
        }

        public Builder titleText(String title) {
            this.titleText = title;
            return this;

        }

        public Builder backgroundColor(int backgroundColor) {
            this.backgroundColor = backgroundColor;
            return this;
        }

        public Builder placeHolder(int placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public Builder customView(int customviewId, int imageViewId, int handlerId, int titleId) {

            this.customViewId = customviewId;
            this.imageViewId = imageViewId;
            this.handlerId = handlerId;
            this.titleId = titleId;
            return this;

        }

        public AndroidImagePicker build() {
            return new AndroidImagePicker(this);

        }

    }

}


