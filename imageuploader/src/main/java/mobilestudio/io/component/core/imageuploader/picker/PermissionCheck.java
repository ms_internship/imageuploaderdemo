package mobilestudio.io.component.core.imageuploader.picker;

import android.app.Activity;
import android.app.Fragment;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sayed on 8/15/2017.
 */

public class PermissionCheck implements IPermissionCheck {
    private final int PERMISSION_REQUEST_CODE = 1;
    private Activity activity;
    private PermissionCallback callback;
    private List<String> permissionList;

    public PermissionCheck(Fragment fragment, PermissionCallback callback) {
        this(fragment.getActivity(), callback);
    }

    public PermissionCheck(Activity activity, PermissionCallback callback) {
        this.activity = activity;
        this.callback = callback;
        permissionList = setPermission();
    }

    boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }


    public List<String> setPermission() {
        List<String> per = new ArrayList<>();
        try {
            PackageManager pm = activity.getApplicationContext().getPackageManager();
            PackageInfo pi = pm.getPackageInfo(activity.getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            String permissionInfo[] = pi.requestedPermissions;
            for (String p : permissionInfo) {
                per.add(p);
            }
        } catch (Exception e) {

        }
        return per;
    }

    private void returnResults() {
        List<String> grantedList = new ArrayList<>();
        List<String> deniedList = new ArrayList<>();
        for (String permission : permissionList) {

            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED)
                deniedList.add(permission);
                else
                grantedList.add(permission);

        }
        callback.onPermissionResult(grantedList , deniedList);
    }

    @Override
    public void requestPermissions(String... permissionsRequested) {
        if (isMarshmallow()) {
            for (String permString : permissionsRequested) {

                if (ContextCompat.checkSelfPermission(activity,
                        permString)
                        != PackageManager.PERMISSION_GRANTED) {
                   // Toast.makeText(activityWeakReference.get() , "You have to grant the permssion before using the picker" , Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(activity,
                            new String[]{permString},
                            PERMISSION_REQUEST_CODE);
                } else {
                    List<String> reqList = new ArrayList<String>() ;
                    reqList.add(permString);
                    callback.onPermissionResult(reqList , new ArrayList<String>());
                }
            }
        }
        //returnResults();
    }


    @Override
    public void onPermissionRequestResult(int requestCode, String[] permissions, int[] grantResults) {

        List<String> grantedList = new ArrayList<>();
        List<String> deniedList = new ArrayList<>();


        for (int i = 0; i < permissions.length; ++i) {
            String perm = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                grantedList.add(perm);
            else
                deniedList.add(perm);

        }
        callback.onPermissionResult(grantedList, deniedList);

    }


}
