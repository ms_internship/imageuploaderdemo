package mobilestudio.io.component.core.imageuploaderdemo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import mobilestudio.io.component.core.imageuploader.IImageUploader;
import mobilestudio.io.component.core.imageuploader.ImageUploader;
import mobilestudio.io.component.core.imageuploader.ImageUploaderCallback;
import mobilestudio.io.component.core.imageuploader.picker.AndroidImagePicker;
import mobilestudio.io.component.core.imageuploader.picker.AndroidPicker;
import mobilestudio.io.component.core.imageuploader.uploader.SingleFirebaseUploader;
import mobilestudio.io.component.core.imageuploader.uploader.Uploader;


public class CustomViewUploaderDemo extends AppCompatActivity implements ImageUploaderCallback {
    IImageUploader imageUploader;
    AndroidPicker imagePicker;
    Uploader firebaseUploader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customviewuploaderdemo);
        imagePicker = new AndroidImagePicker.Builder(this)
                 .customView(R.id.root , R.id.image_selected , R.id.handler ,R.id.image_title  )
               // .defaultView(R.id.viewid)
                .build();
        firebaseUploader = new SingleFirebaseUploader(this);
        imageUploader = new ImageUploader(imagePicker, firebaseUploader);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.common ,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.defaultitem)
        {
            Intent intent = new Intent(this , DefaultViewUploaderDemo.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageUploader.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imageUploader.onPermissionRequestResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onUploadSuccess() {
        Toast.makeText(this, "Upload Success ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUploadFailed(String errorMessage) {
        Toast.makeText(this, "Upload Failed , " + errorMessage, Toast.LENGTH_LONG).show();
    }

    public void Upload(View view) {

        String fileName = "Sayed";
        imageUploader.upload("gs://imageuploaderdemo.appspot.com/" + fileName);

    }
}